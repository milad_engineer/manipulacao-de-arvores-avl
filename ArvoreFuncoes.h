/* Universidade Cat�lica Dom Bosco
Curso de Engenharia da Computa��o
Professor Marcos Alves
Aluno Milad Roghanian, RA 160985
Trabalho de Estrutura de dados I, �rvores AVL
Data de finaliza��o do trabalho 08/12/2016*/

#include <iostream>
#include <cstdlib>

using namespace std;

///STRUCT DO NOARVORE
struct Noarvore
{
    int info, bal;
    Noarvore *Eprox;
    Noarvore *Dprox;
};
///A RAIZ QUE APONTA PARA UM NO ARVORE
struct arvore
{
    Noarvore *raiz;
};

///INICIALIZA ARVORE
void inicializa_arvore (arvore &a)
{
    a.raiz=NULL;
}

///VERIFICA A ARVORE VAZIA
bool arvore_vazia(arvore &a)
{
    return (a.raiz==NULL);
}

///ALOCA E INSERE NO NA ARVORE
void InsereNo(arvore &a, int x)
{
    a.raiz = new Noarvore();
    a.raiz->info = x;
    a.raiz->Eprox = NULL;
    a.raiz->Dprox = NULL;
    a.raiz->bal = 0;
}

///ROTA��O A DIREITA (SIMPLES E DUPLA)
void RotacaoDireita (arvore &a, bool &h)
{
    Noarvore *arvore_aux, *arvore_aux2;
    arvore_aux = a.raiz->Eprox;
    if (arvore_aux->bal==-1) //Caso 1.1 rota��o a direita
    {
        a.raiz->Eprox = arvore_aux->Dprox;
        arvore_aux->Dprox = a.raiz;
        a.raiz->bal=0;
        a.raiz = arvore_aux;
    } else { //Caso 1.2 arvore_aux->bal==1, rota��o dupla a direita
        arvore_aux2 = arvore_aux->Dprox;
        arvore_aux->Dprox = arvore_aux2->Eprox;
        arvore_aux2->Eprox = arvore_aux;
        a.raiz->Eprox = arvore_aux2->Dprox;
        arvore_aux2->Dprox = a.raiz;

        if (arvore_aux2->bal==0) //realiza o balanceamento
        {
            arvore_aux->bal=0;
            a.raiz->bal=0;

        } else if (arvore_aux2->bal==-1) {
            arvore_aux->bal=0;
            a.raiz->bal=1;
        } else {
            arvore_aux->bal=-1;
            a.raiz->bal=0;
        }
        a.raiz = arvore_aux2;
    }
    a.raiz->bal=0;
    h=false;
}

///ROTA��O A ESQUERDA (SIMPLES E DUPLA)
void RotacaoEsquerda (arvore &a, bool &h)
{
    Noarvore *arvore_aux, *arvore_aux2;
    arvore_aux = a.raiz->Dprox;
    if (arvore_aux->bal==1) //Caso 2.1 rota��o a esquerda
    {
        a.raiz->Dprox = arvore_aux->Eprox;
        arvore_aux->Eprox = a.raiz;
        a.raiz->bal=0;
        a.raiz = arvore_aux;
    } else { //Caso 1.2 arvore_aux.raiz->bal==1, rota��o dupla a direita
        arvore_aux2 = arvore_aux->Eprox;
        arvore_aux->Eprox = arvore_aux2->Dprox;
        arvore_aux2->Dprox = arvore_aux;
        a.raiz->Dprox = arvore_aux2->Eprox;
        arvore_aux2->Eprox = a.raiz;

        if (arvore_aux2->bal==1) //realiza o balanceamento
        {
            a.raiz->bal=-1;
            arvore_aux->bal=0;

        } else if (arvore_aux2->bal==0) {
            arvore_aux->bal=0;
            a.raiz->bal=0;
        } else {
            arvore_aux->bal=1;
            a.raiz->bal=0;
        }
        a.raiz = arvore_aux2;
    }
    a.raiz->bal=0;
    h=false;
}

///IMPRIME ARVORE
void imprime_arvore (Noarvore *a, int space)
{
    if (a!=NULL) {//enquanto a arvore for diferente de nulo
        imprime_arvore(a->Dprox, space+1); //entra na direita
        for (int i=1; i<=space; i++) //
        {
            cout<<" - ";
        }
        cout<<a->info; //depois imprime o no
        cout<<endl;
        imprime_arvore(a->Eprox,space+1);//e entra na esquerda
    }
}
