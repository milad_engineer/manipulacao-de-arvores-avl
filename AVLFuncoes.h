/* Universidade Cat�lica Dom Bosco
Curso de Engenharia da Computa��o
Professor Marcos Alves
Aluno Milad Roghanian, RA 160985
Trabalho de Estrutura de dados I, �rvores AVL
Data de finaliza��o do trabalho 08/12/2016*/

#include <iostream>
#include <cstdlib>
#include "ArvoreFuncoes.h"

using namespace std;

///FUN��O BUSCA UM NO NA ARVORE
//Essa fun��o procura de forma recursiva se um determinado no x passado por parametro existe na �rvore
//e o retorna na forma de ponteiro para aquele no
Noarvore* BuscaNo (arvore &a, int x)
{
    arvore arvore_aux;
    if (a.raiz==NULL) { //Se o n� n�o for vazio
        return NULL;
    } else {
        arvore_aux = a; //a arvore � passada para um auxiliar
        while (arvore_aux.raiz!=NULL && arvore_aux.raiz->info!=x) //enquanto o no n�o for vazio e n�o for o valor procurado
        {
            if (x<arvore_aux.raiz->info) {
                arvore_aux.raiz = arvore_aux.raiz->Eprox; //entra de forma recursiva na esquerda
            } else {
                arvore_aux.raiz = arvore_aux.raiz->Dprox; //entra de forma recursiva na direita
            }
        }
    }
    return arvore_aux.raiz;
}

///FUN��O INSERE UM NO NA ARVORE
//Essa fun��o procura o local adequado para inserir o novo no na �rvore de forma que os balanceamentos sejam realizados
//H s� recebe true se a �rvore for modificada
void InsereAVL(arvore &a, int x, bool &h)
{
    arvore arvore_aux;
    if (arvore_vazia(a)) { //Se o n� for vazio ele � inserido na �rvore
        InsereNo(a,x);
        h=true;
    } else if (x < a.raiz->info) { //Se o valor a ser inserido for menor que o no
        arvore_aux.raiz = a.raiz->Eprox;
        InsereAVL(arvore_aux,x,h);
        a.raiz->Eprox=arvore_aux.raiz;
        if (h) { // a sub-�rvore � esquerda de p foi alterada
            switch (a.raiz->bal)
            {
                case 1: //Caso esteja desbalanceado a direita
                a.raiz->bal=0; //recebe 0 pois est� balanceado com a inser��o a esquerda
                h=false;
                break;

                case 0: //caso esteja balanceado
                a.raiz->bal=-1; //recebe -1 pois est� desbalanceado com a inser��o a esquerda
                break;

                case -1: //caso j� esteja desbalanceado a esquerda
                RotacaoDireita(a, h); //faz-se uma rota��o a Direita
                break;
            }
        }
    } else if (x > a.raiz->info){ //Se o valor a ser inserido for maior que o no
        arvore_aux.raiz = a.raiz->Dprox;
        InsereAVL(arvore_aux,x,h);
        a.raiz->Dprox=arvore_aux.raiz;
        if (h) { // a sub-�rvore � direita de p foi alterada
            switch (a.raiz->bal)
            {
                case -1:
                a.raiz->bal=0;
                h=false;
                break;

                case 0:
                a.raiz->bal=1;
                break;

                case 1:
                RotacaoEsquerda(a,h);
                break;
            }
        }
    }
}

///FUN��O REMOVE UM NO NA ARVORE
//Essa fun��o procura o local em que se encontra o no que se deseja remover e o remove de maneira adequada
//realizando os balanceamentos quando necess�rio
//H s� recebe true se a �rvore for modificada
void RemoveAVL (arvore &a, int x, bool &h)
{
    arvore arvore_aux, arvore_aux2;
    if (a.raiz!=NULL) { //Se o n� n�o for vazio
         if (a.raiz->info<x){ //se o no que se quer inserir for maior que o atual no
            arvore_aux.raiz = a.raiz->Dprox;
            RemoveAVL(arvore_aux,x,h); //entra na recurs�o a direita
            a.raiz->Dprox=arvore_aux.raiz;
            if (h && a.raiz!=NULL) { // a sub-�rvore � direita de a foi alterada
                switch (a.raiz->bal)
                {
                    case 1: //Caso o no j� esteja desbalanceado na direita
                    a.raiz->bal=0; //com a remo��o de um no a direita ele fica balanceado
                    h=false;
                    break;

                    case 0: //Caso o no j� esteja balanceado
                    a.raiz->bal=-1; //o no fica desbalanceado a esquerda com a remo��o de um no da direita
                    break;

                    case -1: //caso o no j� esteja desbalanceado � esquerda
                    a.raiz->Eprox->bal=-1; //o no fica desbalanceado a esquerda dele com a remo��o de um no da direita
                    RotacaoDireita(a,h);
                    break;
                }
            }
        } else if (a.raiz->info>x) { //se o no que se quer inserir for menor que o atual no
            arvore_aux.raiz = a.raiz->Eprox;
            RemoveAVL(arvore_aux,x,h); //entra na recurs�o a esquerda
            a.raiz->Eprox=arvore_aux.raiz;
            if (h && a.raiz!=NULL) { // a sub-�rvore � esquerda de a foi alterada
                switch (a.raiz->bal)
                {
                    case 1:
                    a.raiz->bal=0;
                    h=false;
                    break;

                    case 0:
                    a.raiz->Dprox->bal=1;
                    RotacaoEsquerda(a,h);
                    break;

                    case -1:
                    a.raiz->bal=1;
                    break;
                }
            }
        } else { //a.raiz->info==x
            if (a.raiz->Dprox==NULL){ //caso a direita esteja vazia ele receber� a esquerda (caso seja nulo tamb�m)
                a.raiz = a.raiz->Eprox;
                h=true;
            } else if (a.raiz->Eprox==NULL) { //caso a direita n�o esteja vazia e a esquerda vazia ele receber� a direita
                a.raiz = a.raiz->Dprox;
                h=true;
            } else { //caso o no tenha filhos dos dois lados
                Noarvore *p,*r;
                p = a.raiz->Dprox; //p recebe a direita do n�
                r = a.raiz; //r aponta para o n�
                h=true;
                while (p->Eprox!=NULL)
                {
                    r = p; //r percorrer� sempre atr�s de p
                    p = p->Eprox; //p percorrer� a esquerda at� achar a folha mais a esquerda
                }
                a.raiz->info = p->info; //o no recebe o valor de p (a folha mais a esquerda da direita do no)
                if (a.raiz==r) { //caso o no seja nulo na esquerda do no a sua direira
                    r->Dprox = p->Dprox; //a direita do no recebeira a direita
                } else {
                    r->Eprox = p->Dprox; //caso n�o, o no receber� os filhos da direita do n� a esquerda
                    if (h && a.raiz!=NULL) { // a sub-�rvore � direita de a foi alterada
                        switch (a.raiz->bal)
                        {
                            case 1:
                            a.raiz->bal=0;
                            h=false;
                            break;

                            case 0:
                            a.raiz->bal=-1;
                            break;

                            case -1:
                            a.raiz->Eprox->bal=-1;
                            RotacaoDireita(a,h);
                            break;
                        }
                    }
                }
            }
        }
    }
}

/// FUN��O VALOR M�XIMO
//Essa fun��o busca o valor mais a direita da �rvore que � o maior da �rvore
int ValMaximo (Noarvore *a)
{
    if (a==NULL){
        return NULL;
    } else {
        if (a->Dprox!=NULL) {
            ValMaximo(a->Dprox);
        } else {
            return a->info;
        }
    }
}

/// FUN��O VALOR MINIMO
//Essa fun��o busca o valor mais a esquerda da �rvore que � o menor da �rvore
int ValMinimo (Noarvore *a)
{
    if (a==NULL){
        return NULL;
    } else {
        if (a->Eprox!=NULL) {
            ValMinimo(a->Eprox);
        } else {
            return a->info;
        }
    }
}


