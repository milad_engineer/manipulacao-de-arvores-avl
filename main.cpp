/* Universidade Cat�lica Dom Bosco
Curso de Engenharia da Computa��o
Professor Marcos Alves
Aluno Milad Roghanian, RA 160985
Trabalho de Estrutura de dados I, �rvores AVL
Data de finaliza��o do trabalho 08/12/2016*/

#include <iostream>
#include <cstdlib>
#include "AVLFuncoes.h"

using namespace std;

int  main()
{
    int op=-1, x;
    bool h, cria=false;
    arvore Araiz;
    Noarvore *Noarvore_aux;

    ///MENU COM AS OP��ES DE A��ES �RVORE
    while (op != 0)
    {
        system("cls");
        cout<<"|-------------------------|\n"
            <<"|###### ARVORE AVL #######|\n"
            <<"|                         |\n"
            <<"|1 - CRIA ARVORE          |\n"
            <<"|2 - INSERE NA ARVORE     |\n"
            <<"|3 - REMOVE DA ARVORE     |\n"
            <<"|4 - IMPRIME              |\n"
            <<"|5 - MAXIMO VALOR         |\n"
            <<"|6 - MINIMO VALOR         |\n"
            <<"|7 - DELETA ARVORE        |\n"
            <<"|0 - SAIR                 |\n"
            <<"|-------------------------|\n"
            <<"Digite a opcao: ";
        cin>>op;

        switch (op)
        {
            //INICIALIZA A ARVORE
            case 1:
            inicializa_arvore(Araiz);
            cria = true;
            break;

            //CASO A ARVORE EXISTA ELE INSERE NA ARVORE
            case 2:
            if (cria) {
                cout<<"Digite o valor a inserir: ";
                cin>>x;
                InsereAVL(Araiz,x,h);
            } else {
                cout<<"\n    ATENCAO!, ARVORE NAO EXISTE\n\n";
                system("pause");
            }
            break;

            //CASO A ARVORE EXISTA ELE DELETA UM NO DA ARVORE
            case 3:
            if (cria) {
                cout<<"Digite o valor a remover: ";
                cin>>x;
                if (BuscaNo(Araiz,x)!=NULL) { //Busca-se o valor para ver se existe na �rvore
                    RemoveAVL(Araiz,x,h); //caso existir ele � removido
                    cout<<endl;
                    cout<<"\n Valor "<<x<<" removido da arvore"<<endl;
                    system("pause");
                } else { //caso n�o, exibe a mensagem
                    cout<<"\n      NO NAO ENCONTRADO   \n\n";
                    system("pause");
                }
                cout<<endl;
            } else {
                cout<<"\n    ATENCAO!, ARVORE NAO EXISTE\n\n";
                system("pause");
            }
            break;

            //CASO A ARVORE EXISTA ELE A IMPRIME
            case 4:
            if (cria) {
                system("cls");
                if (Araiz.raiz==NULL){ //Caso a arvore esteja vazia ele exibe a mensagem
                    cout<<"\n      ARVORE VAZIA   \n\n";
                } else { //Caso n�o, imprime-se a �rvore
                    imprime_arvore(Araiz.raiz,0);
                }
            cout<<endl;
            system("pause");
            } else {
                cout<<"\n    ATENCAO!, ARVORE NAO EXISTE\n\n";
                system("pause");
            }
            break;

            //CASO A ARVORE EXISTA ELE MOSTRA O NO DE MAIOR VALOR DA �RVORE
            case 5:
            if (cria) {
                if (Araiz.raiz!=NULL){
                    int x = ValMaximo(Araiz.raiz);
                    cout<<"\n O maior valor da arvore e "<<x;
                } else {
                    cout<<"\n      ARVORE VAZIA   \n\n";
                }
            cout<<endl;
            system("pause");
            } else {
                cout<<"\n    ATENCAO!, ARVORE NAO EXISTE\n\n";
                system("pause");
            }
            break;

            //CASO A ARVORE EXISTA ELE MOSTRA O NO DE MENOR VALOR DA �RVORE
            case 6:
            if (cria) {
                if (Araiz.raiz!=NULL){
                    int x = ValMinimo(Araiz.raiz);
                    cout<<"\n O menor valor da arvore e "<<x;
                } else {
                    cout<<"\n      ARVORE VAZIA   \n\n";
                }
            cout<<endl;
            system("pause");
            } else {
                cout<<"\n    ATENCAO!, ARVORE NAO EXISTE\n\n";
                system("pause");
            }
            break;

            //DELETA-SE A �RVORE ANULANDO SUA RAIZ COM O INICIALIZAR
            case 7:
            inicializa_arvore(Araiz);
            cout<<"\n    ARVORE DELETADA POR COMPLETO\n\n";
            system("pause");
            cria=false;
            break;

            case 0:
            break;

            //CASO O USU�RIO INSIRA UMA OP��O INEXISTENTE NO MENU
            default:
            cout<<"\n    OPCAO INVALIDA, DIGITE NOVAMENTE\n\n";
            system("pause");
            break;
        }
    }
}
